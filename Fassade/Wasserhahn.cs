﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fassade.Properties;

namespace Fassade
{
    public partial class Wasserhahn : UserControl
    {
        bool on = true;
        public Wasserhahn()
        {
            InitializeComponent();
        }

        
        public void TurnOn()
        {
            this.BackgroundImage = Resources.whOn;
        }

        public void TurnOff()
        {
            this.BackgroundImage = Resources.whOff;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            TurnOn();
            if (on)
                TurnOff();
            on = !on;
        }
    }
}
