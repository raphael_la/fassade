﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fassade.Properties;

namespace Fassade
{
    public partial class Heizung : UserControl
    {
        bool full = true;
        public Heizung()
        {
            InitializeComponent();
        }

        public void Up()
        {
            this.lblDisplay.Height += 20;
        }

        public void Down()
        {
            this.lblDisplay.Height -= 20;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Up();
            if (full)
                Down();
            full = !full;
        }
    }
}
