﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fassade
{
    public partial class Form1 : Form
    {
        Steuerung s;
        public Form1()
        {
            InitializeComponent();
            s = new Steuerung(this.Controls);
        }

        private void wasserhahn1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void lampe1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        bool on = true;
        private void button1_Click(object sender, EventArgs e)
        {
            if (on)
                s.TurnOff();
            else
                s.TurnOn();
            on = !on;
        }
    }
}
