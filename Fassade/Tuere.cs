﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fassade.Properties;

namespace Fassade
{
    public partial class Tuere : UserControl
    {
        bool open = true;
        public Tuere()
        {
            InitializeComponent();
        }

        public void Open()
        {
            this.BackgroundImage = Resources.doorOpen;
        }

        public void Close()
        {
            this.BackgroundImage = Resources.doorClosed;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Open();
            if (open)
                Close();
            open = !open;
        }
    }
}
