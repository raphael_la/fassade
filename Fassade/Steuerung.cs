﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.Control;

namespace Fassade
{
    class Steuerung
    {
        List<Lampe> lampen = new List<Lampe>();
        List<Heizung> heizungen = new List<Heizung>();
        List<Tuere> tueren = new List<Tuere>();
        List<Wasserhahn> wasserHaehne = new List<Wasserhahn>();
        List<wifi> wifi = new List<wifi>();

        public Steuerung(ControlCollection controls)
        {
            foreach(var c in controls)
            {
                if (c is Lampe lampe)
                    lampen.Add(lampe);
                else if (c is Wasserhahn w)
                    wasserHaehne.Add(w);
                else if (c is Tuere t)
                    tueren.Add(t);
                else if (c is Heizung h)
                    heizungen.Add(h);
                else if (c is wifi wi)
                    wifi.Add(wi);
            }
        }

        public void TurnOff()
        {
            lampen.ForEach(x => x.TurnOff());
            heizungen.ForEach(x => x.Down());
            tueren.ForEach(x => x.Close());
            wasserHaehne.ForEach(x => x.TurnOff());
            wifi.ForEach(x => x.TurnOff());
        }

        public void TurnOn()
        {
            lampen.ForEach(x => x.TurnOn());
            heizungen.ForEach(x => x.Up());
            tueren.ForEach(x => x.Open());
            wasserHaehne.ForEach(x => x.TurnOn());
            wifi.ForEach(x => x.TurnOn());
        }
    }
}
