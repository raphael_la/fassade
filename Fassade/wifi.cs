﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fassade.Properties;

namespace Fassade
{
    public partial class wifi : UserControl
    {
        bool on = true;
        public wifi()
        {
            InitializeComponent();
        }

        public void TurnOn()
        {
            this.BackgroundImage = Resources.wifiOn;
        }

        public void TurnOff()
        {
            this.BackgroundImage = Resources.wifiOff;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            TurnOn();
            if (on)
                TurnOff();
            on = !on;
        }
    }
}
