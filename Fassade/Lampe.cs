﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fassade.Properties;

namespace Fassade
{
    public partial class Lampe : UserControl
    {
        bool on = true;
        public Lampe()
        {
            InitializeComponent();
        }

        public void TurnOn()
        {
            this.BackgroundImage = Resources.light_on;
        }

        public void TurnOff()
        {
            this.BackgroundImage = Resources.light_off;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            TurnOn();
            if (on)
                TurnOff();
            on = !on;
        }
    }
}
