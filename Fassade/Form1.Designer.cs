﻿namespace Fassade
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.wasserhahn1 = new Fassade.Wasserhahn();
            this.lampe1 = new Fassade.Lampe();
            this.lampe2 = new Fassade.Lampe();
            this.lampe3 = new Fassade.Lampe();
            this.lampe4 = new Fassade.Lampe();
            this.lampe5 = new Fassade.Lampe();
            this.tuere1 = new Fassade.Tuere();
            this.heizung1 = new Fassade.Heizung();
            this.button1 = new System.Windows.Forms.Button();
            this.wifi1 = new Fassade.wifi();
            this.wifi2 = new Fassade.wifi();
            this.SuspendLayout();
            // 
            // wasserhahn1
            // 
            this.wasserhahn1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wasserhahn1.BackgroundImage")));
            this.wasserhahn1.Location = new System.Drawing.Point(544, 478);
            this.wasserhahn1.Name = "wasserhahn1";
            this.wasserhahn1.Size = new System.Drawing.Size(31, 32);
            this.wasserhahn1.TabIndex = 0;
            this.wasserhahn1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.wasserhahn1_MouseClick);
            // 
            // lampe1
            // 
            this.lampe1.BackColor = System.Drawing.Color.Transparent;
            this.lampe1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lampe1.BackgroundImage")));
            this.lampe1.Location = new System.Drawing.Point(246, 110);
            this.lampe1.Name = "lampe1";
            this.lampe1.Size = new System.Drawing.Size(44, 51);
            this.lampe1.TabIndex = 1;
            this.lampe1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lampe1_MouseClick);
            // 
            // lampe2
            // 
            this.lampe2.BackColor = System.Drawing.Color.Transparent;
            this.lampe2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lampe2.BackgroundImage")));
            this.lampe2.Location = new System.Drawing.Point(391, 134);
            this.lampe2.Name = "lampe2";
            this.lampe2.Size = new System.Drawing.Size(44, 51);
            this.lampe2.TabIndex = 2;
            // 
            // lampe3
            // 
            this.lampe3.BackColor = System.Drawing.Color.Transparent;
            this.lampe3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lampe3.BackgroundImage")));
            this.lampe3.Location = new System.Drawing.Point(206, 291);
            this.lampe3.Name = "lampe3";
            this.lampe3.Size = new System.Drawing.Size(44, 51);
            this.lampe3.TabIndex = 3;
            // 
            // lampe4
            // 
            this.lampe4.BackColor = System.Drawing.Color.Transparent;
            this.lampe4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lampe4.BackgroundImage")));
            this.lampe4.Location = new System.Drawing.Point(516, 300);
            this.lampe4.Name = "lampe4";
            this.lampe4.Size = new System.Drawing.Size(44, 51);
            this.lampe4.TabIndex = 4;
            // 
            // lampe5
            // 
            this.lampe5.BackColor = System.Drawing.Color.Transparent;
            this.lampe5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lampe5.BackgroundImage")));
            this.lampe5.Location = new System.Drawing.Point(135, 411);
            this.lampe5.Name = "lampe5";
            this.lampe5.Size = new System.Drawing.Size(44, 51);
            this.lampe5.TabIndex = 5;
            // 
            // tuere1
            // 
            this.tuere1.BackColor = System.Drawing.Color.Transparent;
            this.tuere1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tuere1.BackgroundImage")));
            this.tuere1.Location = new System.Drawing.Point(614, 434);
            this.tuere1.Name = "tuere1";
            this.tuere1.Size = new System.Drawing.Size(55, 76);
            this.tuere1.TabIndex = 6;
            // 
            // heizung1
            // 
            this.heizung1.BackColor = System.Drawing.Color.Transparent;
            this.heizung1.Location = new System.Drawing.Point(452, 478);
            this.heizung1.Name = "heizung1";
            this.heizung1.Size = new System.Drawing.Size(17, 49);
            this.heizung1.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(691, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // wifi1
            // 
            this.wifi1.BackColor = System.Drawing.Color.Transparent;
            this.wifi1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wifi1.BackgroundImage")));
            this.wifi1.Location = new System.Drawing.Point(542, 372);
            this.wifi1.Name = "wifi1";
            this.wifi1.Size = new System.Drawing.Size(32, 31);
            this.wifi1.TabIndex = 9;
            // 
            // wifi2
            // 
            this.wifi2.BackColor = System.Drawing.Color.Transparent;
            this.wifi2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wifi2.BackgroundImage")));
            this.wifi2.Location = new System.Drawing.Point(206, 458);
            this.wifi2.Name = "wifi2";
            this.wifi2.Size = new System.Drawing.Size(32, 31);
            this.wifi2.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Fassade.Properties.Resources.haus;
            this.ClientSize = new System.Drawing.Size(799, 603);
            this.Controls.Add(this.wifi2);
            this.Controls.Add(this.wifi1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.heizung1);
            this.Controls.Add(this.tuere1);
            this.Controls.Add(this.lampe5);
            this.Controls.Add(this.lampe4);
            this.Controls.Add(this.lampe3);
            this.Controls.Add(this.lampe2);
            this.Controls.Add(this.lampe1);
            this.Controls.Add(this.wasserhahn1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Wasserhahn wasserhahn1;
        private Lampe lampe1;
        private Lampe lampe2;
        private Lampe lampe3;
        private Lampe lampe4;
        private Lampe lampe5;
        private Tuere tuere1;
        private Heizung heizung1;
        private System.Windows.Forms.Button button1;
        private wifi wifi1;
        private wifi wifi2;
    }
}

